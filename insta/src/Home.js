import React, { Component } from 'react';
import './App.css';

import Post from './components/Post';
import Comment from './components/Comment';
import Navbar from './components/Navbar';
import PostList from './components/PostList.js'

const Posts = [{
  title: "titre",
  desc: "description",
  img: "https://images3.alphacoders.com/915/915614.jpg"
}];

class Home extends Component {
  render() {
    return (
      <div className="Home">
        <Navbar/>
        <div class="row grey lighten-2">
          <div class="col s6 offset-s3">
            <PostList/>
            

          </div>
        </div>
      </div>
    );
  }
}

export default Home;
