import React, { Component } from 'react';
import './App.css';

import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './Home.js';
import LoginPage from './LoginPage.js';

const Posts = [{
  title: "titre",
  desc: "description",
  img: "https://images3.alphacoders.com/915/915614.jpg"
}];

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/Login' component={LoginPage}/>
          {/*<Route path='/roster' component={Roster}/>
          <Route path='/schedule' component={Schedule}/>*/}
        </Switch>
      </BrowserRouter>
    );
  }
  
}

export default App;
