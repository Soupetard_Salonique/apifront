import React from 'react';

import axios from 'axios';
import Post from './Post.js'

export default class PostList extends React.Component {
  constructor() {
    super();
    this.state = {
      posts: []
    };
  }

// axios.get('http://localhost:8080/api/posts')
//   .then(res => {
//     console.log(res.data);
//     const posts_ = res.data;
//     this.setState({ posts: posts_ });
//   })

  getPosts = async () => {
      try {
          const options = {
              method: "get",
              headers: {
                  'Content-Type': "application/x-www-form-urlencoded"
              },
              url: "http://localhost:8080/api/posts"
          };
          let res = await axios(options);
          console.log(res);
          this.setState({ posts: res.data.posts });
      } catch (err) {
          console.log(err);
      }
  }

  componentDidMount() {
    this.getPosts();
  }

  render() {
    if (this.state.posts) {
      return (
        <div>
          <ul>
            {this.state.posts.map(post => <li><Post title={post.title} desc={post.desc} image={post.image}/></li>)}
          </ul>
        </div>
      )
    } else {
      return (
        <ul> Invalid Token </ul>
      )
    }
  }
}
