import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class Navbar extends Component{

      render() {
        return (
            <nav>
                <div class="nav-wrapper purple lighten-3 z-depth-2">
                    <Link to='/'><a class="brand-logo"><i class="material-icons">camera_enhance</i>Instazz</a></Link>
                    <ul class="right hide-on-med-and-down">
                        <li><Link to='/Login'><i class="material-icons">account_circle</i></Link></li>
                        <li><a href="#"><i class="material-icons">add_a_photo</i></a></li>
                        <li><a href="#"><i class="material-icons">refresh</i></a></li>
                    </ul>
                </div>
            </nav>

        );
    }
}
