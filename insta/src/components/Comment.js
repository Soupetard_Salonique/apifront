import React, { Component } from 'react';

export default class Comment extends Component{
          
      render() {
        return (
            <div class="valign-wrapper">
                <i class="small   material-icons prefix">account_circle</i>
                <b>{this.props.user}</b>
            </div>
        );
    }
}