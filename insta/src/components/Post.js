import React, { Component } from 'react';
import Comment from "./Comment.js";

export default class Post extends Component{
      getInit() {
            return {comments: Comment};
      }
      render() {
        return (
            <div class="card">
                  <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src={this.props.image}/>
                  </div>
                  <div class="card-content hoverable white">
                        <span class="card-title activator grey-text text-darken-4">{this.props.title}<i class="material-icons right">more_vert</i></span>
                  </div>

                  <div class="card-reveal white">
                        <span class="card-title grey-text text-darken-4">{this.props.title}<i class="material-icons right">close</i></span>
                        <p class="light grey-text text-darken-4">{this.props.desc}</p>

                        <div class="valign-wrapper">
                              <div class="input-field col s12">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input id="icon_prefix" type="text" class="validate"/>
                                    <label for="icon_prefix">Commentaire</label>
                              </div>
                        </div>
                  </div>
            </div>
        );
    }
}
